﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declaration of both envelope's sides variables
            double width1, height1=0, width2=0, height2=0;
            bool correct_numbers;
            
            //Specifying the sides of first envelope and checking the right format- must be only a number
            do
            {
                correct_numbers = false;
                Console.WriteLine("Specify the sides of first envelope" + "\nSpecify width: ");
                string s_width1 = Console.ReadLine();

                if (!Double.TryParse(s_width1, out width1))
                {
                    Console.WriteLine("Enter correct number");
                    correct_numbers = true;
                    continue;
                }

                Console.WriteLine();
                Console.WriteLine("Specify height: ");
                string s_height1 = Console.ReadLine();

                if (!Double.TryParse(s_height1, out height1))
                {
                    Console.WriteLine("Enter correct number");
                    correct_numbers = true;
                    continue;
                }

                //Specifying the sides of second envelope and checking the right format- must be only a number


                Console.WriteLine("Specify the sides of second envelope" + "\nSpecify width: ");
                string s_width2 = Console.ReadLine();

                if (!Double.TryParse(s_width2, out width2))
                {
                    Console.WriteLine("Enter correct number");
                    correct_numbers = true;
                    continue;
                }

                Console.WriteLine();
                Console.WriteLine("Specify height: ");

                string s_height2 = Console.ReadLine();

                if (!Double.TryParse(s_height2, out height2))
                {
                    Console.WriteLine("Enter correct number");
                    correct_numbers = true;
                    continue;
                }
            } while (correct_numbers);
            Console.WriteLine();

            if (((width1 >= width2) & (height1 >= height2))|((width1>=height2)&(height1>=width2)))
                Console.WriteLine("the second envelope can be insertet into first one");
            else if (((width2 > width1) & (height2 > height1))|((width2>height1)&(height2>width1)))
                Console.WriteLine("the first envelope can be inserted into second one");
            //else if --- other conditions...
            else Console.WriteLine("the envelopes can not be inserted into each other");

            Console.ReadKey();

            
            }
        
    }
}
