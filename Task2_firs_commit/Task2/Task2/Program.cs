﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declaration of both envelope's sides variables
            int width1, height1, width2, height2;
            
            //Specifying the sides of first envelope and checking the right format- must be only a number
      
            Console.WriteLine("Specify the sides of first envelope" + "\nSpecify width: ");
            string s_width1 = Console.ReadLine();

            width1 = Convert.ToInt32(s_width1);

            Console.WriteLine();
            Console.WriteLine("Specify height: ");
            string s_height1=Console.ReadLine();

            height1=Convert.ToInt32(s_height1);
            
            //Specifying the sides of second envelope and checking the right format- must be only a number
            
            
                Console.WriteLine("Specify the sides of second envelope" + "\nSpecify width: ");
                string s_width2 = Console.ReadLine();

                width2 = Convert.ToInt32(s_width2);

                Console.WriteLine();
                Console.WriteLine("Specify height: ");

                string s_height2 = Console.ReadLine();

                height2 = Convert.ToInt32(s_height2);
           
            Console.ReadKey();
            if (((width1 >= width2) & (height1 >= height2))|((width1>=height2)&(height1>=width2)))
                Console.WriteLine("the second envelope can be insertet into first one");
            else if (((width2 > width1) & (height2 > height1))|((width2>height1)&(height2>width1)))
                Console.WriteLine("the first envelope can be inserted into second one");
            //else if --- other conditions...
            else Console.WriteLine("the envelopes can not be inserted into each other");

            Console.ReadKey();

            
            }
        
    }
}
